#!/bin/bash

# $1 equals ${lastRelease.version}
# $2 equals ${nextRelease.gitHead}

TMP_DIR=$(mktemp -d)
cd $TMP_DIR || exit
eval $(ssh-agent -s)
echo "$DEPLOY_KEY" | tr -d '\r' | ssh-add - > /dev/null
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan gitlab.gwdg.de  >> ~/.ssh/known_hosts
git config --global user.email "$GIT_AUTHOR_EMAIL"
git config --global user.name "$GIT_AUTHOR_NAME"
git clone git@gitlab.gwdg.de:subugoe/emo/text-api.git
cd text-api || exit

# add entry in menu
echo "" >> config.toml
echo '[[menu.main]]' >> config.toml
echo "    name = '$1'"  >> config.toml
echo "    url = 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/tree/$2/content/page'" >> config.toml
echo '    parent = "archive"' >> config.toml

# commit result
git status
git add --all
git commit -m "update versions"
git push
