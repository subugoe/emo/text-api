"""
  validation tool for Text API
"""

import json
import os
from re import findall

import click
import jsonschema


@click.command()
@click.option('--all', is_flag=True, help='Validate the complete sample server.')
@click.option('--file', '-f', help='Validate a file given at a file path.')
def validate(all: bool, file: str):
    """
    A simple programm that either validates all JSON files in sample_server/
    or validates a specific item/manifest/collection.json
    """
    if all:
        for file in get_all_sample_files():
            validate_file(file)

    elif file:
        validate_file(file)

    else:
        click.echo('Unknown parameter. See --help for details.')


def get_all_sample_files() -> list:
    """
    prepares a list of all files
    """
    path =r'sample_server'
    list_of_files = []

    for root, dirs, files in os.walk(path):
        for file in files:
            list_of_files.append(os.path.join(root,file))
    return list_of_files


def validate_file(file_path):
    """
    validates a single file
    """
    print(f'ℹ️   currently validating {file_path}…')
    data = get_json_file(file_path)
    entity_type = determine_type(file_path)
    schema = get_schema(entity_type)
    this_dir = os.path.dirname( os.path.realpath(__file__) )
    refres = jsonschema.RefResolver(referrer=schema, base_uri='file://' + this_dir + '/' )

    jsonschema.validate(data, schema, resolver=refres)

    print(f'💖   successfully validated {file_path}')


def get_json_file(file_path) -> dict:
    """
    returns a dictionary containing data from a JSON file
    """
    with open(file_path, 'r', encoding='utf-8') as jsondata:
        return json.load(jsondata)


def get_schema(entity_type) -> dict:
    """
    returns a dictionary containing a schema as JSON
    """
    file_path = 'schema/' + entity_type + '.json'
    with open(file_path, 'r', encoding='utf-8') as jsondata:
        return json.load(jsondata)


def determine_type(file_path) -> str:
    """
    returns the entity type (collection, manifest, or item) of a given file
    """
    pattern = r'collection|manifest|item'
    return findall(pattern, file_path)[0]

if __name__ == '__main__':
    validate()
