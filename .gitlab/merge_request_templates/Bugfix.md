# Bug fix

Well, it seems that it is impossible to write software without bugs.
That's why we are really happy that you are here.
Thank you very much!
Please take a moment to help us include the purposed bug fix by filling out the following form.

Feel free to delete sections that doesn't suit the case you're dealing with.

## Summary

Apart from what is mentioned in the main ticket you are going to close with this
MR, tell us what you have done to achieve this goal.

## Related Tickets

Add all related issues and especially those to be closed.
Keep in mind that every `bugfix` branch needs an issue that properly describes the bug beforehand.
If your fix addresses something untracked, please open a ticket first.

## Does the result of the MR comply to our "definition of done"?

* [ ] Unit tests passed
* [ ] Code reviewed
* [ ] Acceptance criteria met
* [ ] Functional tests passed
* [ ] Non-Functional requirements met
* [ ] Product Owner accepts the User Story

### Related

### Closes


## Changelog

* [ ] I added a statement to the CHANGELOG.

/cc [Mathias Göbel](https://gitlab.gwdg.de/mgoebel), [Frank Schneider](https://gitlab.gwdg.de/schneider210), [Michelle Weidling](https://gitlab.gwdg.de/mrodzis)
