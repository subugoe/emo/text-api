openapi: '3.0.2'
info:
  title: TextAPI
  version: '1.0.0'
  description: This is a sample server for interchanging textual resources.
  contact:
    name: Göttingen State and University Library
    email: textapi@sub.uni-goettingen.de
  license:
    name: Creative Commons Attribution-NoDerivatives 4.0 International
    url: https://creativecommons.org/licenses/by-nd/4.0/legalcode

servers:
  - url: https://subugoe.pages.gwdg.de/emo/text-api/sample_server/
    description: The URL of the server you implement the TextAPI at.

paths:
  /{collection}/collection.json:
    description: Gives information about a curated list of texts.
    get:
      summary: Returns an object with metadata about this collection as well as references to the collected texts.
      externalDocs:
        url: https://subugoe.pages.gwdg.de/emo/text-api/page/specs/#collection-object
      operationId: getCollection
      parameters:
        - name: collection
          in: path
          description: The identifier of the collection
          schema: {$ref: '#/components/schemas/Collection'}
          required: true
          examples:
            semanticID:
              summary: An identifier that has a meaning for humans
              value: "ShakespearesWorks"
            ID:
              summary: An identifier with no special meaning for humans
              value: "12a34b"
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CollectionObject'
              example: https://subugoe.pages.gwdg.de/emo/text-api/sample_server/collection.json

  /{collection}/{manifest}/manifest.json:
    get:
      summary: Returns an object with metadata about this text as well as references to the manifest's item.
      externalDocs:
        url: https://subugoe.pages.gwdg.de/emo/text-api/page/specs/#manifest-object
      operationId: getManifest
      parameters:
        - name: collection
          in: path
          description: The identifier of the collection
          schema: {$ref: '#/components/schemas/Collection'}
          required: true
          examples:
            semanticID:
              summary: An identifier that has a meaning for humans
              value: "ShakespearesWorks"
            ID:
              summary: An identifier with no special meaning for humans
              value: "12a34b"
        - name: manifest
          in: path
          description: The identifier of the manifest
          schema: {$ref: '#/components/schemas/Manifest'}
          required: true
          examples:
            semanticID:
              summary: An identifier that has a meaning for humans
              value: "Othello"
            ID:
              summary: An identifier with no special meaning for humans
              value: "12a34b"
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ManifestObject'
              example: https://subugoe.pages.gwdg.de/emo/text-api/sample_server/manifest.json


  /{collection}/{manifest}/{item}/{revision}/item.json:
    get:
      summary: Returns an object with metadata about this item at a given revision.
      externalDocs:
        url: https://subugoe.pages.gwdg.de/emo/text-api/page/specs/#item-object
      operationId: getItemAtRevision
      parameters:
        - name: collection
          in: path
          description: The identifier of the collection
          schema: {$ref: '#/components/schemas/Collection'}
          required: true
          examples:
            semanticID:
              summary: An identifier that has a meaning for humans
              value: "ShakespearesWorks"
            ID:
              summary: An identifier with no special meaning for humans
              value: "12a34b"
        - name: manifest
          in: path
          description: The identifier of the manifest
          schema: {$ref: '#/components/schemas/Manifest'}
          required: true
          examples:
            semanticID:
              summary: An identifier that has a meaning for humans
              value: "Othello"
            ID:
              summary: An identifier with no special meaning for humans
              value: "12a34b"
        - name: item
          in: path
          description: The identifier of the item
          schema: {$ref: '#/components/schemas/Item'}
          required: true
          examples:
            semanticID:
              summary: An identifier that has a meaning for humans
              value: "page90"
            ID:
              summary: An identifier with no special meaning for humans
              value: "3x_2"
        - name: revision
          in: path
          description: The revision of the item to be delivered.
          schema: {$ref: '#/components/schemas/Revision'}
          required: true
          examples:
            semanticID:
              summary: A revision that has a meaning for humans
              value: "1"
            ID:
              summary: A revision with no special meaning for humans
              value: "fb406b265a779b43afa635bc8da79d81"
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ItemObject'
              example: https://subugoe.pages.gwdg.de/emo/text-api/sample_server/latest/item.json

 
  /{collection}/{manifest}/{item}/{revision}/full.json:
    get:
      summary: Returns an object with metadata about this full version of a text at a given revision.
      externalDocs:
        url: https://subugoe.pages.gwdg.de/emo/text-api/page/specs/#item-object
      operationId: getFullManifestAtRevision
      parameters:
        - name: collection
          in: path
          description: The identifier of the collection
          schema: {$ref: '#/components/schemas/Collection'}
          required: true
          examples:
            semanticID:
              summary: An identifier that has a meaning for humans
              value: "ShakespearesWorks"
            ID:
              summary: An identifier with no special meaning for humans
              value: "12a34b"
        - name: manifest
          in: path
          description: The identifier of the manifest
          schema: {$ref: '#/components/schemas/Manifest'}
          required: true
          examples:
            semanticID:
              summary: An identifier that has a meaning for humans
              value: "Othello"
            ID:
              summary: An identifier with no special meaning for humans
              value: "12a34b"
        - name: item
          in: path
          description: The identifier of the item
          schema: {$ref: '#/components/schemas/Item'}
          required: true
        - name: revision
          in: path
          description: The revision of the item to be delivered.
          schema: {$ref: '#/components/schemas/Revision'}
          required: true
          examples:
            semanticID:
              summary: A revision that has a meaning for humans
              value: "1"
            ID:
              summary: A revision with no special meaning for humans
              value: "fb406b265a779b43afa635bc8da79d81"
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ItemObject'
              example: https://subugoe.pages.gwdg.de/emo/text-api/sample_server/latest/item.json          

components:
  schemas:
    TextAPIVersion:
      type: string
      pattern: '[0-9]+\.[0-9]+\.[0-9]+'
      
    URL:
      type: string
      pattern: '^https?://'

    StringArray:
      type: array
      items:
        type: string

    Collection:
      type: string

    CollectionObject:
      type: object
      required:
      - \@context
      - textapi
      - title
      - collector
      - sequence
      properties:
        \@context:
          type: string
          pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/collection.jsonld'
        textapi:
          $ref: '#/components/schemas/TextAPIVersion'
        title:
          type: array
          items:
            oneOf:
              - $ref: '#/components/schemas/TitleMainObject'
            $ref: '#/components/schemas/TitleObject'
        collector:
          type: array
          items:
            $ref: '#/components/schemas/ActorObject'
        description:
          type: string
        sequence:
          type: array
          items:
            $ref: '#/components/schemas/SequenceObject'
        annotationCollection:
          $ref: '#/components/schemas/URL'
        modules:
          type: array
          items:
            $ref: '#/components/schemas/ModuleObject'

    TitleMainObject:
      type: object
      required:
      - \@context
      - title
      - type
      properties:
        \@context:
          type: string
          pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/title.jsonld'
        title:
          type: string
        type:
          type: string
          pattern: 'main'

    TitleObject:
      type: object
      required:
      - \@context
      - title
      - type
      properties:
        \@context:
          type: string
          pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/title.jsonld'
        title:
          type: string
        type:
          type: string
          pattern: '(main|sub)'

    ActorObject:
      type: object
      required:
      - \@context
      - role
      - name
      properties:
        \@context:
          type: string
          pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/actor.jsonld'
        role:
          $ref: '#/components/schemas/StringArray'
        name:
          type: string
        id:
          type: string
        idref:
          type: array
          items:
            required:
            - \@context
            - type
            - id
            properties:
              \@context:
                type: string
                pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/idref.jsonld'
              base:
                $ref: '#/components/schemas/URL'
              type:
                type: string
              id:
                type: string

    SequenceObject:
      type: object
      required:
      - \@context
      - id
      - type
      properties:
        \@context:
          type: string
          pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/sequence.jsonld'
        id:
          $ref: '#/components/schemas/URL'
        type:
          type: string
          pattern: '(collection|manifest|item)'
        label:
          type: string

    ModuleObject:
      type: object
      properties:
        edition-manuscripts:
          type: boolean
        edition-prints:
          type: boolean

    Manifest:
      type: string

    ManifestObject:
      type: object
      required:
      - \@context
      - textapi
      - id
      - label
      - sequence
      - license
      properties:
        \@context:
          type: string
          pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/manifest.jsonld'
        textapi:
          $ref: '#/components/schemas/TextAPIVersion'
        id:
          $ref: '#/components/schemas/URL'
        label:
          type: string
        sequence:
          type: array
          items:
            $ref: '#/components/schemas/SequenceObject'
        actor:
          type: array
          items:
            $ref: '#/components/schemas/ActorObject'
        repository:
          type: array
          items:
            type: object
            required:
            - \@context
            - url
            - baseUrl
            - id
            properties:
              \@context:
                type: string
                pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/repository.jsonld'
              label:
                type: string
              url:
                $ref: '#/components/schemas/URL'
              baseUrl:
                $ref: '#/components/schemas/URL'
              id:
                type: string
        image:
          $ref: '#/components/schemas/ImageObject'
        metadata:
          type: array
          items:
            $ref: '#/components/schemas/MetadataObject'
        support:
          type: array
          items:
            type: object
            required:
            - \@context
            - type
            - mime
            - url
            properties:
              \@context:
                type: string
                pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/support.jsonld'
              type:
                type: string
                pattern: '(font|css)'
              mime:
                type: string
              url:
                $ref: '#/components/schemas/URL'
        license:
          type: array
          items:
            $ref: '#/components/schemas/LicenseObject'
        description:
          type: string
        annotationCollection:
          $ref: '#/components/schemas/URL'
        modules:
          type: array
          items:
            $ref: '#/components/schemas/ModuleObject'

    ImageObject:
      type: object
      required:
      - \@context
      - id
      - license
      properties:
        \@context:
          type: string
          pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/image.jsonld'
        id:
          $ref: '#/components/schemas/URL'
        manifest:
          $ref: '#/components/schemas/URL'
        license:
          $ref: '#/components/schemas/LicenseObject'

    MetadataObject:
      oneOf:
      - $ref: '#/components/schemas/MetadataObjectValue'
      - $ref: '#/components/schemas/MetadataObjectMetadata'

    MetadataObjectValue:
      type: object
      required:
      - \@context
      - key
      - value
      properties:
        \@context:
          type: string
          pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/metadata-value.jsonld'
        key:
          type: string
        value:
          type: string

    MetadataObjectMetadata:
      type: object
      required:
      - key
      - metadata
      properties:
        key:
          type: string
        metadata:
          type: array
          items:
            $ref: '#/components/schemas/MetadataObject'

    LicenseObject:
      type: object
      required:
      - \@context
      - id
      properties:
        \@context:
          type: string
          pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/license.jsonld'
        id:
          type: string
          externalDocs:
            url: https://spdx.org/licenses/
        notes:
          type: string

    Item:
      type: string

    ItemObject:
      type: object
      required:
      - \@context
      - textapi
      - type
      - lang
      - content
      properties:
        \@context:
          type: string
          pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/item.jsonld'
        textapi:
          $ref: '#/components/schemas/TextAPIVersion'
        title:
          type: array
          items:
            oneOf:
              - $ref: '#/components/schemas/TitleMainObject'
            $ref: '#/components/schemas/TitleObject'
        type:
          type: string
          pattern: '(section|page|full)'
        n:
          type: string
        lang:
          $ref: '#/components/schemas/StringArray'
        langAlt:
          $ref: '#/components/schemas/StringArray'
        content:
          type: array
          items:
            type: object
            required:
            - \@context
            - url
            - type
            properties:
              \@context:
                type: string
                pattern: 'https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/content.jsonld'
              url:
                $ref: '#/components/schemas/URL'
              type:
                type: string
        description:
          type: string
        image:
          $ref: '#/components/schemas/ImageObject'
        annotationCollection:
          $ref: '#/components/schemas/URL'
        modules:
          type: array
          items:
            $ref: '#/components/schemas/ModuleObject'

    Revision:
      type: string
